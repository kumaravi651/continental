import 'package:continetal/models/continental_model.dart';
import 'package:continetal/widgets/list_view_widget.dart';
import 'package:flutter/material.dart';

class HomeComponent extends StatefulWidget {
  final ContinentalModel? continentalData;
  bool? isConnectedToInternet;
  final List<Map<String, dynamic>>? getSqliteDB;
  HomeComponent({
    Key? key,
    this.continentalData,
    this.isConnectedToInternet,
    this.getSqliteDB,
  }) : super(key: key);

  @override
  State<HomeComponent> createState() => _HomeComponentState();
}

class _HomeComponentState extends State<HomeComponent> {
  @override
  Widget build(BuildContext context) {
    return ListViewWidget(
      continentalData: widget.continentalData,
      isConnectedToInternet: widget.isConnectedToInternet,
      fetchSqliteDB: widget.getSqliteDB,
    );
  }
}
