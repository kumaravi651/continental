import 'dart:convert';

import 'package:continetal/models/continental_model.dart';
import 'package:http/http.dart';

class ContientalClient {
  ContientalClient(this._client) {
    _baseUrl = 'https://dl.dropboxusercontent.com';
    _contientalData = "/s/2iodh4vg0eortkl/facts.json";
  }

  late final Client _client;

  late String _baseUrl;
  late String _contientalData;

  Future<ContinentalModel> fetchContinentalData() async {
    final url = _baseUrl + _contientalData;

    final response = await _client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final jsonMap = json.decode(response.body) as Map<String, dynamic>;
      return ContinentalModel.fromJson(jsonMap);
    } else {
      throw Exception('Failed to load movies');
    }
  }
}
