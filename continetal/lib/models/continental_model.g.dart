// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'continental_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContinentalModel _$ContinentalModelFromJson(Map<String, dynamic> json) =>
    ContinentalModel(
      json['title'] as String?,
      (json['rows'] as List<dynamic>?)
          ?.map((e) => ContinentalRows.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ContinentalModelToJson(ContinentalModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'rows': instance.rows,
    };

ContinentalRows _$ContinentalRowsFromJson(Map<String, dynamic> json) =>
    ContinentalRows(
      json['title'] as String?,
      json['description'] as String?,
      json['imageHref'] as String?,
    );

Map<String, dynamic> _$ContinentalRowsToJson(ContinentalRows instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'imageHref': instance.imageHref,
    };
