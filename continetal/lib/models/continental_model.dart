import 'package:json_annotation/json_annotation.dart';
part 'continental_model.g.dart';


@JsonSerializable()
class ContinentalModel {
  String? title;
  List<ContinentalRows>? rows;

  ContinentalModel(
    this.title,
    this.rows,
  );

  factory ContinentalModel.fromJson(Map<String, dynamic> json) =>
      _$ContinentalModelFromJson(json);
  Map<String, dynamic> toJson() => _$ContinentalModelToJson(this);
}

@JsonSerializable()
class ContinentalRows {
  String? title;
  String? description;
  String? imageHref;

  ContinentalRows(
    this.title,
    this.description,
    this.imageHref,
  );

  factory ContinentalRows.fromJson(Map<String, dynamic> json) =>
      _$ContinentalRowsFromJson(json);
  Map<String, dynamic> toJson() => _$ContinentalRowsToJson(this);
}
