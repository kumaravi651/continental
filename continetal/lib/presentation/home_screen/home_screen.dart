import 'dart:io';

import 'package:continetal/application/continental_cubit.dart';
import 'package:continetal/application/continental_state.dart';
import 'package:continetal/components/home_component.dart';
import 'package:continetal/database/db_provider.dart';

import 'package:continetal/widgets/app_bar.dart';
import 'package:continetal/widgets/wid_txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isConnected = false;
  List<Map<String, dynamic>>? sqliteDB;

  // This function is triggered when the floating button is pressed
  Future<void> _checkInternetConnection() async {
    try {
      final response = await InternetAddress.lookup('www.google.com');
      if (response.isNotEmpty) {
        setState(() {
          isConnected = true;
          BlocProvider.of<ContinentalCubit>(context).getContinentalAPI();
        });
      }
    } on SocketException catch (err) {
      setState(() {
        isConnected = false;
      });
      debugPrint(err.toString());
    }
  }

  Future fetchDataFromDB() async {
    sqliteDB = await DatabaseHelper.instance.queryAllRows();
  }

  // This will check the connection at the beginning
  @override
  void initState() {
    _checkInternetConnection();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (isConnected == true)
        ? BlocBuilder<ContinentalCubit, ContinentalState>(
            builder: (context, state) {
              if (state is ContinentalStateInitial) {
                return const Scaffold(
                  appBar: CustomAppBar(),
                );
              }
              if (state is ContinentalStateLoading) {
                return const Scaffold(
                  appBar: CustomAppBar(),
                  body: Center(
                    child: CupertinoActivityIndicator(),
                  ),
                );
              }
              if (state is ContinentalStateSuccess) {
                return Scaffold(
                  appBar: AppBar(
                    title: Text(state.continental.title ?? ""),
                  ),
                  body: HomeComponent(
                    continentalData: state.continental,
                    isConnectedToInternet: isConnected,
                    getSqliteDB: sqliteDB,
                  ),
                );
              }
              return const Scaffold(
                appBar: CustomAppBar(),
              );
            },
          )
        : Scaffold(
            appBar: AppBar(
              title: const Text('Continental'),
            ),
            body: FutureBuilder(
                future: fetchDataFromDB(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                        child: Text('Please wait its loading...'));
                  } else {
                    if (sqliteDB?.isEmpty == true) {
                      return const Center(
                          child: WidText(
                        title: "No Internect Connection",
                        widColor: Colors.blue,
                      ));
                    } else {
                      return HomeComponent(
                        isConnectedToInternet: isConnected,
                        getSqliteDB: sqliteDB,
                      );
                    }
                  }
                }),
          );
  }
}
