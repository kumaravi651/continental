import 'package:continetal/application/continental_cubit.dart';
import 'package:continetal/repository/continental_repository.dart';
import 'package:continetal/services/services.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';

final getIt = GetIt.instance;

void init() {
  registerProductDependencies();
}

void registerProductDependencies() {
  getIt.registerLazySingleton(() => Client());
  getIt.registerFactory(() => ContinentalRepository(getIt()));
  getIt.registerFactory(() => ContientalClient(getIt()));
  getIt.registerFactory(() => ContinentalCubit(getIt()));
}
