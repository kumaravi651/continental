import 'package:continetal/presentation/home_screen/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'application/continental_cubit.dart';
import 'dependecy_injection/depedency_injection.dart';
import 'package:continetal/dependecy_injection/depedency_injection.dart' as di;

void main() async {
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<ContinentalCubit>(),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Continental',
        home: HomeScreen(),
      ),
    );
  }
}
