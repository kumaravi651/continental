import 'package:continetal/models/continental_model.dart';
import 'package:continetal/services/services.dart';

class ContinentalRepository {
  ContinentalRepository(this._continentalClient);

  late final ContientalClient _continentalClient;

  Future<ContinentalModel> fetchContinentalData() =>
      _continentalClient.fetchContinentalData();
}
