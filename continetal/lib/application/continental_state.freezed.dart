// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'continental_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ContinentalStateTearOff {
  const _$ContinentalStateTearOff();

  ContinentalStateInitial initial() {
    return const ContinentalStateInitial();
  }

  ContinentalStateLoading loading() {
    return const ContinentalStateLoading();
  }

  ContinentalStateSuccess success(ContinentalModel continental) {
    return ContinentalStateSuccess(
      continental,
    );
  }

  ContinentalStateError error(String message) {
    return ContinentalStateError(
      message,
    );
  }
}

/// @nodoc
const $ContinentalState = _$ContinentalStateTearOff();

/// @nodoc
mixin _$ContinentalState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContinentalModel continental) success,
    required TResult Function(String message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContinentalStateInitial value) initial,
    required TResult Function(ContinentalStateLoading value) loading,
    required TResult Function(ContinentalStateSuccess value) success,
    required TResult Function(ContinentalStateError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContinentalStateCopyWith<$Res> {
  factory $ContinentalStateCopyWith(
          ContinentalState value, $Res Function(ContinentalState) then) =
      _$ContinentalStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContinentalStateCopyWithImpl<$Res>
    implements $ContinentalStateCopyWith<$Res> {
  _$ContinentalStateCopyWithImpl(this._value, this._then);

  final ContinentalState _value;
  // ignore: unused_field
  final $Res Function(ContinentalState) _then;
}

/// @nodoc
abstract class $ContinentalStateInitialCopyWith<$Res> {
  factory $ContinentalStateInitialCopyWith(ContinentalStateInitial value,
          $Res Function(ContinentalStateInitial) then) =
      _$ContinentalStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContinentalStateInitialCopyWithImpl<$Res>
    extends _$ContinentalStateCopyWithImpl<$Res>
    implements $ContinentalStateInitialCopyWith<$Res> {
  _$ContinentalStateInitialCopyWithImpl(ContinentalStateInitial _value,
      $Res Function(ContinentalStateInitial) _then)
      : super(_value, (v) => _then(v as ContinentalStateInitial));

  @override
  ContinentalStateInitial get _value => super._value as ContinentalStateInitial;
}

/// @nodoc

class _$ContinentalStateInitial implements ContinentalStateInitial {
  const _$ContinentalStateInitial();

  @override
  String toString() {
    return 'ContinentalState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ContinentalStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContinentalModel continental) success,
    required TResult Function(String message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContinentalStateInitial value) initial,
    required TResult Function(ContinentalStateLoading value) loading,
    required TResult Function(ContinentalStateSuccess value) success,
    required TResult Function(ContinentalStateError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class ContinentalStateInitial implements ContinentalState {
  const factory ContinentalStateInitial() = _$ContinentalStateInitial;
}

/// @nodoc
abstract class $ContinentalStateLoadingCopyWith<$Res> {
  factory $ContinentalStateLoadingCopyWith(ContinentalStateLoading value,
          $Res Function(ContinentalStateLoading) then) =
      _$ContinentalStateLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContinentalStateLoadingCopyWithImpl<$Res>
    extends _$ContinentalStateCopyWithImpl<$Res>
    implements $ContinentalStateLoadingCopyWith<$Res> {
  _$ContinentalStateLoadingCopyWithImpl(ContinentalStateLoading _value,
      $Res Function(ContinentalStateLoading) _then)
      : super(_value, (v) => _then(v as ContinentalStateLoading));

  @override
  ContinentalStateLoading get _value => super._value as ContinentalStateLoading;
}

/// @nodoc

class _$ContinentalStateLoading implements ContinentalStateLoading {
  const _$ContinentalStateLoading();

  @override
  String toString() {
    return 'ContinentalState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ContinentalStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContinentalModel continental) success,
    required TResult Function(String message) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContinentalStateInitial value) initial,
    required TResult Function(ContinentalStateLoading value) loading,
    required TResult Function(ContinentalStateSuccess value) success,
    required TResult Function(ContinentalStateError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class ContinentalStateLoading implements ContinentalState {
  const factory ContinentalStateLoading() = _$ContinentalStateLoading;
}

/// @nodoc
abstract class $ContinentalStateSuccessCopyWith<$Res> {
  factory $ContinentalStateSuccessCopyWith(ContinentalStateSuccess value,
          $Res Function(ContinentalStateSuccess) then) =
      _$ContinentalStateSuccessCopyWithImpl<$Res>;
  $Res call({ContinentalModel continental});
}

/// @nodoc
class _$ContinentalStateSuccessCopyWithImpl<$Res>
    extends _$ContinentalStateCopyWithImpl<$Res>
    implements $ContinentalStateSuccessCopyWith<$Res> {
  _$ContinentalStateSuccessCopyWithImpl(ContinentalStateSuccess _value,
      $Res Function(ContinentalStateSuccess) _then)
      : super(_value, (v) => _then(v as ContinentalStateSuccess));

  @override
  ContinentalStateSuccess get _value => super._value as ContinentalStateSuccess;

  @override
  $Res call({
    Object? continental = freezed,
  }) {
    return _then(ContinentalStateSuccess(
      continental == freezed
          ? _value.continental
          : continental // ignore: cast_nullable_to_non_nullable
              as ContinentalModel,
    ));
  }
}

/// @nodoc

class _$ContinentalStateSuccess implements ContinentalStateSuccess {
  const _$ContinentalStateSuccess(this.continental);

  @override
  final ContinentalModel continental;

  @override
  String toString() {
    return 'ContinentalState.success(continental: $continental)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ContinentalStateSuccess &&
            (identical(other.continental, continental) ||
                other.continental == continental));
  }

  @override
  int get hashCode => Object.hash(runtimeType, continental);

  @JsonKey(ignore: true)
  @override
  $ContinentalStateSuccessCopyWith<ContinentalStateSuccess> get copyWith =>
      _$ContinentalStateSuccessCopyWithImpl<ContinentalStateSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContinentalModel continental) success,
    required TResult Function(String message) error,
  }) {
    return success(continental);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
  }) {
    return success?.call(continental);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(continental);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContinentalStateInitial value) initial,
    required TResult Function(ContinentalStateLoading value) loading,
    required TResult Function(ContinentalStateSuccess value) success,
    required TResult Function(ContinentalStateError value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class ContinentalStateSuccess implements ContinentalState {
  const factory ContinentalStateSuccess(ContinentalModel continental) =
      _$ContinentalStateSuccess;

  ContinentalModel get continental;
  @JsonKey(ignore: true)
  $ContinentalStateSuccessCopyWith<ContinentalStateSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContinentalStateErrorCopyWith<$Res> {
  factory $ContinentalStateErrorCopyWith(ContinentalStateError value,
          $Res Function(ContinentalStateError) then) =
      _$ContinentalStateErrorCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class _$ContinentalStateErrorCopyWithImpl<$Res>
    extends _$ContinentalStateCopyWithImpl<$Res>
    implements $ContinentalStateErrorCopyWith<$Res> {
  _$ContinentalStateErrorCopyWithImpl(
      ContinentalStateError _value, $Res Function(ContinentalStateError) _then)
      : super(_value, (v) => _then(v as ContinentalStateError));

  @override
  ContinentalStateError get _value => super._value as ContinentalStateError;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(ContinentalStateError(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ContinentalStateError implements ContinentalStateError {
  const _$ContinentalStateError(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'ContinentalState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ContinentalStateError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  $ContinentalStateErrorCopyWith<ContinentalStateError> get copyWith =>
      _$ContinentalStateErrorCopyWithImpl<ContinentalStateError>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContinentalModel continental) success,
    required TResult Function(String message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContinentalModel continental)? success,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContinentalStateInitial value) initial,
    required TResult Function(ContinentalStateLoading value) loading,
    required TResult Function(ContinentalStateSuccess value) success,
    required TResult Function(ContinentalStateError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContinentalStateInitial value)? initial,
    TResult Function(ContinentalStateLoading value)? loading,
    TResult Function(ContinentalStateSuccess value)? success,
    TResult Function(ContinentalStateError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ContinentalStateError implements ContinentalState {
  const factory ContinentalStateError(String message) = _$ContinentalStateError;

  String get message;
  @JsonKey(ignore: true)
  $ContinentalStateErrorCopyWith<ContinentalStateError> get copyWith =>
      throw _privateConstructorUsedError;
}
