import 'package:continetal/repository/continental_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'continental_state.dart';

class ContinentalCubit extends Cubit<ContinentalState> {
  ContinentalCubit(this._repository) : super(const ContinentalStateInitial());
  late final ContinentalRepository _repository;

  Future<void> getContinentalAPI() async {
    try {
      emit(const ContinentalStateLoading());
      final contiental = await _repository.fetchContinentalData();
      emit(ContinentalStateSuccess(contiental));
    } on Exception catch (e) {
      emit(ContinentalStateError(e.toString()));
    }
  }
}
