import 'package:continetal/models/continental_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'continental_state.freezed.dart';

@freezed
class ContinentalState with _$ContinentalState {
  const factory ContinentalState.initial() = ContinentalStateInitial;
  const factory ContinentalState.loading() = ContinentalStateLoading;
  const factory ContinentalState.success(ContinentalModel continental) =
      ContinentalStateSuccess;
  const factory ContinentalState.error(String message) = ContinentalStateError;
}
